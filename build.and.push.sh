#!/bin/bash
DOCKER_IMAGE="k33g/k3g.utilities"
DOCKER_TAG="1.0.0"

docker build --no-cache -t ${DOCKER_IMAGE}:${DOCKER_TAG} .

docker push ${DOCKER_IMAGE}:${DOCKER_TAG}